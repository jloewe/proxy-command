package cmd

import (
	"errors"
	"github.com/spf13/cobra"
	"gitlab.com/jloewe/proxy"
	"gitlab.com/jloewe/proxy-command/util"
	"gitlab.com/jloewe/proxy/DataType"
	"strconv"
	"strings"
)

// setCmd represents the set command
var setCmd = &cobra.Command{
	Use:   "set [host (webproxy.company.com)] [port (8080)] [bypass (comma separated, optional)]",
	Short: "Sets proxy settings",
	Args: func(cmd *cobra.Command, args []string) error {
		if !util.IsRoot() {
			return errors.New("Please run set with root permissions")
		}

		if len(args) < 2 {
			return errors.New("requires at least two args")
		}

		_, err := strconv.Atoi(args[1])

		if err != nil {
			return errors.New("port is invalid")
		}

		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		pm := proxy.NewManager(nil)
		port, err := strconv.Atoi(args[1])
		if err != nil {
			panic(err)
		}

		if len(args) < 3 {
			pm.Set(datatype.Proxy{Host: args[0], Port: port})
		} else {
			bypass := strings.Split(args[2], ",")
			pm.Set(datatype.Proxy{Host: args[0], Port: port, Bypass: bypass})
		}
	},
}

func init() {
	rootCmd.AddCommand(setCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// setCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// setCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
