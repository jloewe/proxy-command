package cmd

import (
	"errors"
	"gitlab.com/jloewe/proxy"
	"gitlab.com/jloewe/proxy-command/util"

	"github.com/spf13/cobra"
)

// unsetCmd represents the unset command
var unsetCmd = &cobra.Command{
	Use:   "unset",
	Short: "Unsets proxy settings",
	Args: func(cmd *cobra.Command, args []string) error {
		if !util.IsRoot() {
			return errors.New("Please run unset with root permissions")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		pm := proxy.NewManager(nil)
		pm.Unset()
	},
}

func init() {
	rootCmd.AddCommand(unsetCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// unsetCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// unsetCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
