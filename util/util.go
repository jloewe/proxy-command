package util

import (
	"log"
	"os/exec"
	"strconv"
)

// https://www.socketloop.com/tutorials/golang-force-your-program-to-run-with-root-permissions
func IsRoot() bool {
	// #nosec
	output, err := exec.Command("id", "-u").CombinedOutput()

	if err != nil {
		log.Fatal(err)
	}

	// 0 = root, 501 = non-root user
	i, err := strconv.Atoi(string(output[:len(output)-1]))

	if err != nil {
		log.Fatal(err)
	}

	return i == 0
}
