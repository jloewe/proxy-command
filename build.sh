#!/usr/bin/env bash

function build() {
  export GOOS=$1
  export GOARCH=$2
  echo "Building $GOOS-$GOARCH"
  go build -ldflags="-w -s" -o bin/proxy-$GOOS-$GOARCH
}

# darwin / linux - 32bit / 64bit
for GOOS in darwin linux; do
  for GOARCH in 386 amd64 arm arm64; do
    build $GOOS $GOARCH
  done
done
