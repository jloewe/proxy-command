package main

import (
	"gitlab.com/jloewe/proxy-command/cmd"
)

func main() {
	cmd.Execute()
}
