# proxy-command

CLI Interface for [https://gitlab.com/jloewe/proxy](https://gitlab.com/jloewe/proxy)

## Installation

```sh
wget -O - https://gitlab.com/jloewe/proxy-command/raw/master/install.sh | bash
```

:zap: ODER

```sh
curl https://gitlab.com/jloewe/proxy-command/raw/master/install.sh | bash
```


## Usage

Set a proxy:
```sh
proxy set <host (webproxy.company.com)> <port (8080)> <bypass (comma separated, optional)>
```

Unset proxy:
```sh
proxy unset
```
