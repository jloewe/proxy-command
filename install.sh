#!/usr/bin/env bash

OS=`uname -s`
case $OS in
    'Linux')    OS="linux" ;;
    'Darwin')   OS="darwin" ;;
    *)
        echo "Unknown OS = $OS"
        exit 1
    ;;
esac

ARCH=`uname -m`
case $ARCH in
    i386|i686)  ARCH="386" ;;
    x86_64)     ARCH="amd64" ;;
    arm*)       ARCH="arm" ;;
    *)
        echo "Unknown ARCH = $ARCH"
        exit 1
    ;;
esac

echo "Downloading $OS/$ARCH"

PROXYSCRIPT="proxy-$OS-$ARCH"

if hash curl 2>/dev/null; then
    echo "Downloading with curl."
    curl -k -L --request GET -o $PROXYSCRIPT "https://gitlab.com/jloewe/proxy-command/-/jobs/artifacts/master/raw/bin/$PROXYSCRIPT?job=compile"
else
    if hash wget 2>/dev/null; then
        echo "Downloading with wget."
        wget -O $PROXYSCRIPT "https://gitlab.com/jloewe/proxy-command/-/jobs/artifacts/master/raw/bin/$PROXYSCRIPT?job=compile"
    else
        echo "Please install curl or wget."
        exit 1
    fi
fi

sudo mv $PROXYSCRIPT /usr/bin/proxy
sudo chmod +x /usr/bin/proxy

echo "You can now use the proxy command"
echo "Usage: proxy help"
